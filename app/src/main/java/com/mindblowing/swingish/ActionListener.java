package com.mindblowing.swingish;

/**
 * Created by phil on 2/7/2017.
 */

public interface ActionListener
{
	void actionPerformed(ActionListener.ActionEvent ev);
	class ActionEvent
	{

	}
}
